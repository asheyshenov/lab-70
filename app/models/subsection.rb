class Subsection < ApplicationRecord
    has_many :posts
    belongs_to :section
end

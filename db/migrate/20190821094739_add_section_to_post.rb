class AddSectionToPost < ActiveRecord::Migration[5.2]
  def change
    add_reference :posts, :section, foreign_key: true
  end
end

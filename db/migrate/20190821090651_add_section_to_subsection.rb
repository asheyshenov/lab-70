class AddSectionToSubsection < ActiveRecord::Migration[5.2]
  def change
    add_reference :subsections, :section, foreign_key: true
  end
end
